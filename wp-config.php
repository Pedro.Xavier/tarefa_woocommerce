<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pXmqb+DOJo9koKBciZ5cbtYEsd6XNrCoQMUQkFMapjBCyHNX5pfGKNGQpK1wiEyUWp56xgeN2ThAX9YmBCFhTw==');
define('SECURE_AUTH_KEY',  'RPzybgwRr59GtdT1DiU35ga1yvE6ZN3J2XsbXg7XnQlgz2Q0/icHOuae+0KoHZMOVEn6MGeeUX4CJF6vjVTWxQ==');
define('LOGGED_IN_KEY',    'v+JhJ+Ac1JQcc9kdrdMC1+mXVpzkKrniMFu0OTdISZ2GLXuuF8XOgDb9Ck7eP/XpdyY0nVmXkS5RcpVrqfbEcg==');
define('NONCE_KEY',        'qDc/EL9BFLzvYTtO9KbcFMDKOLv4U1XcP/V1N5yyW2DxiLHmVLeMg+Q5sSROrTyv8Se+/BVHiNey6KO2z3zXTw==');
define('AUTH_SALT',        'FHMH2SxUXq5y0fLmDR5tuLkwHWB+mLb4rGpUktDQkfTnCor43Y8UL7CgOweSVxw7pY03C6Qmd+bO7xzm6jP9LQ==');
define('SECURE_AUTH_SALT', 'LqeOLGt73Vcu1ZD5f3EbH7yaeH2ziXwGNkZ6oouyuYTJC03MrzLfWLzr7hZeskrXlom9xwkx7kFHWSwYgjILYw==');
define('LOGGED_IN_SALT',   'd+Fj7z0LmtrWa2JSpHR1MFRO1epLqLu5vV0D2J+Wg6aFsjbCjYviadprP4Mcyb5GyDbFeZAYKnNy9bIQsZVhwQ==');
define('NONCE_SALT',       'FCKQWRITVVyLGIygSw11kV0OxisBkxGmvRLMFlqnfvz4GS5DY/g0a/CFlumaKnwK4s1K2+CU+BSoiCrwURcnLQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
